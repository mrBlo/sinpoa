package com.blo.sinpoa;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import gr.net.maroulis.library.EasySplashScreen;

public class Splash extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View easySplashScreenView = new EasySplashScreen(Splash.this)
                .withFullScreen()
                .withTargetActivity(Home.class)
                .withSplashTimeOut(2000)
                .withBackgroundResource(R.color.colorAccent)
                //.withHeaderText("Header")
                // .withFooterText("Loading..")
                //  .withBeforeLogoText("My cool company")
                .withLogo(R.drawable.splash_logo)
                //.withAfterLogoText("READ'E")
                .create();

        setContentView(easySplashScreenView);
        getSupportActionBar().hide();
    }


}
