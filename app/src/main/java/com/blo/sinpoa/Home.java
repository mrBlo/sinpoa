package com.blo.sinpoa;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

public class Home extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Navigate to Login or Register Page", Snackbar.LENGTH_LONG)
                        .setActionTextColor(Color.parseColor("#5DA6EF"))
                        .setAction("Proceed", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Toast.makeText(Home.this, "Please Wait", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(Home.this, SignInOrUp.class));
                            }
                        }).show();
            }
        });
    }

}
